# HB Dashboard Rapid Redux
--- 

### How to use 
- ##### run  ```yarn```
- ##### open `package.json` 
- ##### Create a start script under scripts
  <details><summary>See Example</summary>
  <p>

    ```json
    "start:{{your_name}}": "ROOT_PATH={{ABSOLUTE_PATH_OF_DASHBOARD}} node src/index.js" 
    ```

    for more reference see `package.json`
  </p>
  </details>
- ##### Run `yarn start:{{your_name}}`


---

### EXAMPLE

![Screeenshot of the CLI](/screenshots/test.png)
