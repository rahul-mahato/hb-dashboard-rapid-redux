const { SEPARATORS, PATHS, ACTION_TYPES } = require('../constants');
const path = require('path');
const { pathUtils, fs, arrUtils } = require('../utils');
const { PARTIALS } = require('../templates');
const _ = require('lodash');
const { IMPORT } = require('../constants/separators');

const addToFile = async ({ filePath, separator, content }) => {
  try {

    const fileContent = (await fs.readFile(filePath)) || '';
    const sagaDeclarationStartPoint = fileContent.indexOf(separator);

    const importLists = fileContent.slice(0, sagaDeclarationStartPoint - 1).split('\n');
    const sagaDeclarationList = fileContent.slice(sagaDeclarationStartPoint).split('\n');

    const importLastPos = arrUtils.findLastIndexInArray(importLists, SEPARATORS.IMPORT) + 1;

    importLists.splice(importLastPos, 0, content.import);
    sagaDeclarationList.splice(2, 0, content.name);
    const updatedFileContent = [...importLists, ...sagaDeclarationList].join('\n');

    await fs.writeFile(filePath, updatedFileContent);
    return { success: true };
  } catch (error) {
    console.log(error);
    return { success: false, error };
  }
};


const addSagaToIndex = async (name) => {
  const sagaRoot = PARTIALS.getSagaRoot(name);
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.SAGAS.ROOT);

  const data = await addToFile({
    filePath,
    separator: SEPARATORS.SAGAS.INDEX_SAGA_LIST_PREFIX,
    content: sagaRoot
  });

  if (data.error) {

    throw new Error(`Could not create Actions ${JSON.stringify(data.error)}`);
  }
};


exports.addSagaFile = async (name, { type }) => {
  const sagaFileContent = PARTIALS.getSagaFileContent(name, { type });
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.SAGAS.BASE, `${_.camelCase(name)}.js`);
  const { success } = await fs.writeFile(filePath, sagaFileContent);
  if (success) {
    await addSagaToIndex(name);
  } else {
    throw new Error(`Could not create Saga ${JSON.stringify(error)}`);
  }
};

