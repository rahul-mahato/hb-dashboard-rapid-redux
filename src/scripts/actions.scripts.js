const { SEPARATORS, PATHS, ACTION_TYPES } = require('../constants');
const path = require('path');
const { pathUtils, fs, arrUtils } = require('../utils');
const { PARTIALS } = require('../templates');


const addToFile = async ({ filePath, separator, type, content }) => {
  try {

    const fileContent = (await fs.readFile(filePath)) || '';
    const syncActionsStartPoint = fileContent.indexOf(separator);
    const asyncActionsList = fileContent.slice(0, syncActionsStartPoint - 1).split('\n');
    const syncActionsList = fileContent.slice(syncActionsStartPoint).split('\n');

    const asyncActionPos = arrUtils.findLastIndexInArray(asyncActionsList, 'export const') + 1;
    const syncActionPos = arrUtils.findLastIndexInArray(syncActionsList, 'export const') + 1;
    if (type === ACTION_TYPES.ASYNC) {
      asyncActionsList.splice(asyncActionPos, 0, content);
    } else {
      syncActionsList.splice(syncActionPos, 0, content);
    }
    const updatedFileContent = [...asyncActionsList, ...syncActionsList].join('\n');

    await fs.writeFile(filePath, updatedFileContent);
    return { success: true };
  } catch (error) {
    return { success: false, error };
  }
};

exports.addActionType = async (name, { type }) => {
  const actionType = PARTIALS.getActionType(name, { type });
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.ACTIONS.TYPES);

  const data = await addToFile({
    filePath,
    separator: SEPARATORS.ACTION_TYPES.SYNC,
    type,
    content: actionType
  });

  if (data.error) {
    throw new Error(`Could not create Action Type ${JSON.stringify(error)}`);
  }
};

exports.addAction = async (name, { type }) => {
  const actionCreator = PARTIALS.getAction(name, { type });
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.ACTIONS.CREATOR);

  const data = await addToFile({
    filePath,
    separator: SEPARATORS.ACTIONS.SYNC,
    type,
    content: actionCreator
  });

  if (data.error) {
    throw new Error(`Could not create Actions ${JSON.stringify(error)}`);
  }
};