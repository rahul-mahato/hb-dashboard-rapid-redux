const { SEPARATORS, PATHS } = require('../constants');
const path = require('path');
const { pathUtils, fs, arrUtils } = require('../utils');
const { PARTIALS } = require('../templates');
const _ = require('lodash');


const addToFile = async ({ filePath, separator, content }) => {
  try {

    const fileContent = (await fs.readFile(filePath)) || '';
    const fileContentList = fileContent.split('\n');

    const exportLastPos = arrUtils.findLastIndexInArray(fileContentList, separator) + 1;

    fileContentList.splice(exportLastPos, 0, content.export);
    const updatedFileContent = fileContentList.join('\n');

    await fs.writeFile(filePath, updatedFileContent);
    return { success: true };
  } catch (error) {
    console.log(error);
    return { success: false, error };
  }
};


const addSelectorToIndex = async (name) => {
  const selectorRoot = PARTIALS.getSelectorRoot(name);
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.SELECTORS.ROOT);

  const data = await addToFile({
    filePath,
    separator: SEPARATORS.SELECTORS.INDEX_SELECTOR_LIST_PREFIX,
    content: selectorRoot
  });

  if (data.error) {
    throw new Error(`Could not updated Selector ${JSON.stringify(data.error)}`);
  }
};


exports.addSelectorFile = async (reducerName, reducerStates) => {
  const selectorFileContent = PARTIALS.getSelectorFileContent(reducerName, reducerStates);
  const fileName = `${_.camelCase(reducerName)}.js`;
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.SELECTORS.BASE, fileName);
  const { success } = await fs.writeFile(filePath, selectorFileContent);
  if (success) {
    await addSelectorToIndex(reducerName);

  } else {
    throw new Error(`Could not create Saga ${JSON.stringify(error)}`);
  }
};



