const { SEPARATORS, PATHS } = require('../constants');
const path = require('path');
const { pathUtils, fs, arrUtils } = require('../utils');
const { PARTIALS } = require('../templates');
const _ = require('lodash');


const addToFile = async ({ filePath, separator, content }) => {
  try {

    const fileContent = (await fs.readFile(filePath)) || '';
    const reducerDeclarationStartPoint = fileContent.indexOf(separator);

    const importLists = fileContent.slice(0, reducerDeclarationStartPoint - 1).split('\n');
    const reducerDeclarationList = fileContent.slice(reducerDeclarationStartPoint).split('\n');

    const importLastPos = arrUtils.findLastIndexInArray(importLists, SEPARATORS.IMPORT) + 1;

    importLists.splice(importLastPos, 0, content.import);
    reducerDeclarationList.splice(2, 0, content.name);
    const updatedFileContent = [...importLists, ...reducerDeclarationList].join('\n');

    await fs.writeFile(filePath, updatedFileContent);
    return { success: true };
  } catch (error) {
    console.log(error);
    return { success: false, error };
  }
};

const addReducerToIndex = async (name) => {
  const reducerRoot = PARTIALS.getReducerRoot(name);
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.REDUCERS.ROOT);

  const data = await addToFile({
    filePath,
    separator: SEPARATORS.REDUCERS.INDEX_REDUCERS_LIST_PREFIX,
    content: reducerRoot
  });
  if (data.success) {
    return data;
  } else if (data.error) {
    throw new Error(`Could not create Actions ${JSON.stringify(data.error)}`);
  }
};


exports.addReducerFile = async (reducerName) => {
  const reducerFileContent = PARTIALS.getReducerFileContent(reducerName);
  const fileName = `${_.camelCase(reducerName)}.js`;
  const root = pathUtils.getRootPath();
  const filePath = path.join(root, PATHS.REDUCERS.BASE, fileName);
  const { success } = await fs.writeFile(filePath, reducerFileContent);
  if (success) {
    await addReducerToIndex(reducerName);
  } else {
    throw new Error(`Could not create Reducer ${JSON.stringify(error)}`);
  }
};
