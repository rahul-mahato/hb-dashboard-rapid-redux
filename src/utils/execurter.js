exports.execSequence = async (iterators, callback) => {
  return iterators.reduce(async (accum, data) => {
    await accum;
    return callback(data);
  }, Promise.resolve());
};