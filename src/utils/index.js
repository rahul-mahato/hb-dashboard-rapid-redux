exports.inquirerMethods = require('./setup');
exports.pathUtils = require('./path.util');
exports.fs = require('./file');
exports.arrUtils = require('./array.utils');
exports.nameUtils = require('./name.utils');
exports.execSync = require('./execurter').execSequence;