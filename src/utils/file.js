const fs = require('fs');
const path = require('path');

exports.readFile = (path, opts = 'utf8') =>
  new Promise((resolve, reject) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) reject(err);
      else resolve(data.toString());
    });
  });

exports.writeFile = (path, data, opts = 'utf8') =>
  new Promise((resolve, reject) => {
    const pathSplit = path.split('/');
    const dir = pathSplit.slice(0, pathSplit.length - 1).join('/');

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }
    fs.writeFile(path, data, opts, (err) => {
      if (err) reject(err);
      else {
        resolve({ success: true });
      }
    });

  });