
exports.getRootPath = () => {
  return process.env.ROOT_PATH || process.cwd();
};
