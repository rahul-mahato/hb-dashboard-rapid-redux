exports.findLastIndexInArray = (arr, text) => {
  let lastIndex = -1;
  arr.forEach((element, indx) => {
    if (element.includes(text)) {
      lastIndex = indx;
    }
  });
  return lastIndex;
};