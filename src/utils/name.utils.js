const { ACTION_TYPES, SUFFIXES } = require('../constants');

exports.getActionName = (name, { type }) => {
  if (type === ACTION_TYPES.ASYNC) {
    return `${name}${SUFFIXES.ASYNC_ACTIONS}`;
  }
  return name;
};