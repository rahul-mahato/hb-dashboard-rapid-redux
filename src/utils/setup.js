const inquirer = require('inquirer');
const { ACTION_TYPES, CHOICES, STATE_TYPES } = require('../constants');

exports.handleSetup = async () => {
  const rootPath = process.env.ROOT_PATH;
  if (!rootPath) {
    const { useCurrDir } = await inquirer.prompt({
      type: 'confirm',
      name: 'useCurrDir',
      message: `You didn't pass any ROOT_PATH env, is ${process.cwd()} your dashboard rootpath ?`,
      default: true
    });
    if (!useCurrDir) {
      return {
        stopSetup: true
      };
    }
  }
  return await inquirer
    .prompt([
      {
        type: 'list',
        name: 'actionType',
        choices: [ACTION_TYPES.SYNC, ACTION_TYPES.ASYNC],
        message: 'Choose your redux action type:',
      },
      {
        type: 'input',
        name: 'actionName',
        message: 'What would you like to call your action [ if multiple add comma separator ]:',
      },
      {
        type: 'input',
        name: 'reducerName',
        message: 'What would you call your reducer as? [ leave empty if reducer not needed ]',
        default: ''
      }
    ]);
};


exports.getChoice = async ({ message }) => {
  const { choice } = await inquirer.prompt({
    type: 'list',
    name: 'choice',
    choices: Object.values(CHOICES),
    message,
  });
  return choice === CHOICES.YES;
};

exports.getSagaGeneratorActions = async (choices) => {
  const { sagaActions } = await inquirer.prompt({
    type: 'checkbox',
    name: 'sagaActions',
    message: 'Please select the actions for which you need sagas',
    choices
  });
  return sagaActions;
};


exports.getState = async () => {
  const { name, type, needMore } = await inquirer.prompt([{
    type: 'input',
    name: 'name',
    message: "what would you call your state variable"
  }, {
    type: 'list',
    name: 'type',
    message: 'Select the type of your state variable',
    choices: Object.values(STATE_TYPES)
  }, {
    type: 'confirm',
    name: 'needMore',
    message: 'Would you like to add more states ? ',
    default: true
  }]);
  return { name, type, needMore };
};


exports.getSelectorsFileName = async () => {
  return await inquirer
    .prompt({
      type: 'input',
      name: 'selectorName',
      message: 'What would you like to call your selector file:',
    });
};