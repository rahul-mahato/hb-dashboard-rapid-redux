module.exports = {
  ACTION_TYPES: {
    ASYNC: '// Async types',
    SYNC: '// Sync types'
  },
  ACTIONS: {
    ASYNC: '// Async Actions',
    SYNC: '// Sync Actions'
  },
  SAGAS: {
    INDEX_SAGA_LIST_PREFIX: 'const sagas =',
    DECLARATION_BRACKET_CLOSE: '];'
  },
  REDUCERS: {
    INDEX_REDUCERS_LIST_PREFIX: 'export default {',
    DECLARATION_BRACKET_CLOSE: '};'
  },
  SELECTORS: {
    INDEX_SELECTOR_LIST_PREFIX: 'export * from',
  },
  IMPORT: 'import '
};