exports.PATHS = require('./paths');
exports.SEPARATORS = require('./separators');
exports.MODULE_NAME = 'HB\nDashboard\nRapid Redux';
exports.ACTION_TYPES = {
  SYNC: 'sync',
  ASYNC: 'async'
};


exports.SUFFIXES = {
  ASYNC_ACTIONS: '_ASYNC'
};

exports.CHOICES = {
  YES: 'yes',
  NO: 'no'
};

exports.STATE_TYPES = {
  MAP: 'Map',
  LIST: 'List',
  STRING: 'String',
  BOOL: 'Bool'
};

exports.ORA_MESSAGES = {
  ACTIONS: {
    start: 'Generating Actions',
    succeed: 'Actions Generated'
  },
  SELECTOS: {
    start: 'Generating Selectors',
    succeed: 'Selectors Generated'
  },
  SAGAS: {
    start: 'Generating Sagas',
    succeed: 'Sagas Generated'
  },
  REDUCERS: {
    start: 'Generating Reducer',
    succeed: 'Reducer Generated'
  },
};

exports.IMPORTABLE_IMMUTABLE_TYPES = ['Map', 'List'];