exports.SAGAS = {
  ROOT: '/src/sagas/index.js',
  BASE: '/src/sagas/'
};

exports.ACTIONS = {
  TYPES: '/src/actions/types.js',
  CREATOR: '/src/actions/index.js'
};

exports.SELECTORS = {
  BASE: 'src/selectors',
  ROOT: 'src/selectors/index.js'
};


exports.REDUCERS = {
  BASE: 'src/reducers',
  ROOT: 'src/reducers/index.js'
};