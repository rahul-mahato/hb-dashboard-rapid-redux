const actionTemplates = require('./action.templates');
const sagasTemplates = require('./sagas.templates');
const selectorTemplates = require('./selectors.templates');
const reducerTemplates = require('./reducer.templates');

module.exports = {
  ...actionTemplates,
  ...sagasTemplates,
  ...selectorTemplates,
  ...reducerTemplates
};