const { ACTION_TYPES, SUFFIXES } = require('../../constants');
const { nameUtils } = require('../../utils');
const _ = require('lodash');

exports.getSagaFileContent = (name, { type }) => {
  return `import { takeLatest } from 'redux-saga/effects';
import { ${nameUtils.getActionName(name, { type })} } from 'actions/types';

function* ${_.camelCase(name)}Saga({ payload }) {
  console.log({ payload });
}

export default function*() {
  yield takeLatest(${nameUtils.getActionName(name, { type })}.PENDING, ${_.camelCase(name)}Saga);
}
`;
};

exports.getSagaRoot = (name) => {
  const sagaName = _.camelCase(name);

  return {
    import: `import ${sagaName} from './${sagaName}';`,
    name: `  ${sagaName},`
  };
};
