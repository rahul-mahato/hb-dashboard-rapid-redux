const { ACTION_TYPES, SUFFIXES } = require('../../constants');
const _ = require('lodash');

exports.getActionType = (name, { type }) =>
({
  [ACTION_TYPES.SYNC]: `export const ${name} = '${name}';`,
  [ACTION_TYPES.ASYNC]: `export const ${name}_ASYNC = createAsyncTypes('${name}${SUFFIXES.ASYNC_ACTIONS}');`,
}[type]);


exports.getAction = (name, { type }) => ({
  [ACTION_TYPES.ASYNC]: `export const ${_.camelCase(name)} = createAsyncAction(types.${name}${SUFFIXES.ASYNC_ACTIONS});`,
  [ACTION_TYPES.SYNC]: `export const ${_.camelCase(name)} = createAction(types.${name});`,
}[type]); 
