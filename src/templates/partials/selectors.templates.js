const _ = require('lodash');
const { STATE_TYPES, IMPORTABLE_IMMUTABLE_TYPES } = require('../../constants');

const getDefaultValueBasedOnType = (type) => ({
  [STATE_TYPES.MAP]: `${STATE_TYPES.MAP}()`,
  [STATE_TYPES.LIST]: `${STATE_TYPES.LIST}()`,
  [STATE_TYPES.STRING]: `\'\'`,
  [STATE_TYPES.BOOL]: false
}[type]);

const getBaseImportForSelector = () => `import { createSelector } from 'reselect';`;
const getTypeImportsForStates = (types) => `import { fromJS, ${types.join(', ')} } from 'immutable/dist/immutable';`;

const setter = (reducerName, stateName) => `export const set${_.upperFirst(stateName)} = (${reducerName}State, payload) =>
  ${reducerName}State.setIn(['${stateName}'], fromJS(payload));`;

const getter = (reducerName, stateName, type) => `export const get${_.upperFirst(stateName)} = createSelector(
  state => state.getIn(['${reducerName}', '${stateName}'], ${getDefaultValueBasedOnType(type)}),
  ${stateName} => ${stateName}.toJS()
);
`;

const getSetterAndGetter = (reducerName, { name, type }) => {
  return [setter(reducerName, name), getter(reducerName, name, type)].join('\n');
};

exports.getSelectorFileContent = (reducerName, reducerStates) => {
  const stateTypes = [...new Set(reducerStates.map(({ type }) => type))].filter(stateType => IMPORTABLE_IMMUTABLE_TYPES.includes(stateType));
  const settersAndGetters = reducerStates.reduce((accum, { name, type }) => {
    return `${accum}
${getSetterAndGetter(reducerName, ({ name, type }))}`;
  }, '');
  return [
    getBaseImportForSelector(),
    getTypeImportsForStates(stateTypes),
    settersAndGetters
  ].join('\n');
};

exports.getSelectorRoot = (name) => {
  const selectorName = _.camelCase(name);

  return {
    export: `export * from './${selectorName}';`,
    name: `  ${selectorName},`
  };
};