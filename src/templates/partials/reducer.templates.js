const { ACTION_TYPES, SUFFIXES } = require('../../constants');
const { nameUtils } = require('../../utils');
const _ = require('lodash');

exports.getReducerFileContent = (name) => {
  return `import { fromJS } from 'immutable';
import Debug from 'debug';
import { } from 'selectors';
import { } from 'actions/types';

// eslint-disable-next-line no-unused-vars
const debug = Debug('hb:dashboard:reducers:${name}');

// eslint-disable-next-line no-unused-vars
const noop = type => () => {};

const initialState = fromJS({ });

const reducer = (state, { type, payload }) => ({
  // add your actions and selectors here
}[type] || noop(type))() || state;

export default (state = initialState, action) => reducer(state, action);
`;
};

exports.getReducerRoot = (name) => {
  const reducerName = _.camelCase(name);

  return {
    import: `import ${reducerName} from './${reducerName}';`,
    name: `  ${reducerName},`
  };
};
