#! /usr/bin/env node
const { ACTION_TYPES, CHOICES, ORA_MESSAGES, MODULE_NAME } = require('./constants');
const { actionScripts, sagaScripts, selectorScripts, reducerScripts } = require('./scripts');
const { inquirerMethods, execSync } = require('./utils');
const ora = require('ora');
const chalk = require('chalk');
var figlet = require('figlet');
const _ = require('lodash');

const init = (async () => {
  const spinner = ora();
  const asciiArt = figlet.textSync(MODULE_NAME, {
    font: 'ANSI Shadow',
    verticalLayout: "controlled smushing",
    width: 100,
    whitespaceBreak: false
  });

  console.log(`\n\n${chalk.green(asciiArt)}\n\n`);

  try {
    const {
      actionType,
      actionName,
      reducerRequired,
      stateVariables,
      reducerName = '',
      stopSetup
    } = await inquirerMethods.handleSetup();

    if (stopSetup) {
      spinner.fail(`Please run this in your dashboard's terminal or pass ROOT_PATH={{your_dashboard_root_path}} while executing this module`);
      return;
    }

    const reducerNeeded = reducerName.length > 0;
    const reducerStates = [];


    const actionListSplitted = actionName.split(',').map(_.toUpper);

    let addState = await inquirerMethods.getChoice({
      message: 'Would you like to add states setters and selectors ?'
    });
    if (addState) {
      const { selectorName } = await inquirerMethods.getSelectorsFileName();
    }
    while (addState) {
      const { name, type, needMore } = await inquirerMethods.getState();
      reducerStates.push({ name, type });
      addState = needMore;
    }



    let shouldCreateSaga = await inquirerMethods.getChoice({
      message: 'Would you like to create sagas for some of these actions:',
    });


    const sagaActionsList = shouldCreateSaga ? await inquirerMethods.getSagaGeneratorActions(actionListSplitted) : [];


    // Creating Action Types and Actions
    spinner.start(ORA_MESSAGES.ACTIONS.start);
    await execSync(actionListSplitted, (action) => {
      return actionScripts.addActionType(action, { type: actionType });
    });

    await execSync(actionListSplitted, async (action) => {
      return actionScripts.addAction(action, { type: actionType });
    });
    spinner.succeed(ORA_MESSAGES.ACTIONS.succeed);


    // Creating Selectors
    if (reducerNeeded && reducerStates.length > 0) {
      spinner.start(ORA_MESSAGES.SELECTOS.start);
      await selectorScripts.addSelectorFile(reducerNeeded ? reducerName : selectorName, reducerStates);
      spinner.succeed(ORA_MESSAGES.SELECTOS.succeed);
    }


    // Creating Reducer
    if (reducerNeeded) {
      spinner.start(ORA_MESSAGES.REDUCERS.start);
      await reducerScripts.addReducerFile(reducerName);
      spinner.succeed(ORA_MESSAGES.REDUCERS.succeed);
    }

    // Creating Sagas
    if (shouldCreateSaga) {
      spinner.start(ORA_MESSAGES.SELECTOS.start);
      execSync(sagaActionsList, (action) => {
        return sagaScripts.addSagaFile(action, { type: actionType });
      });
      spinner.succeed(ORA_MESSAGES.SELECTOS.succeed);
    }


  } catch (error) {
    console.log(error);
    console.log(JSON.stringify({ error }, undefined, 2));
  }
})();